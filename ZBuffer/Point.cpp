#include "Point.h"

Point::Point( )
{
	for ( int i = 0; i < 3; i++ )
	{
		arr[i] = 0;
	}
}

Point::Point( double x, double y, double z )
{
	arr[0] = x;
	arr[1] = y;
	arr[2] = z;
}

Point Point::getIntersectionPointBetweenAtY( Point p1, Point p2, double yValue )
{
	double x1 = p1.arr[0];
	double x2 = p2.arr[0];

	double y1 = p1.arr[1];
	double y2 = p2.arr[2];

	double z1 = p1.arr[2];
	double z2 = p2.arr[2];

	double m = ( x1 - x2 ) / ( y1 - y2 );
	double x = m * ( yValue - y1 ) + x1;

	m = ( z1 - z2 ) / ( y1 - y2 );
	double z = m * ( yValue - y1 ) + z1;

	Point p( x, yValue, z );

	return p;
}

double Point::getZValue( Point p1, Point p2, double xValue )
{
	double x1 = p1.arr[0];
	double x2 = p2.arr[0];

	double z1 = p1.arr[2];
	double z2 = p2.arr[2];

	double m = ( z1 - z2 ) / ( x1 - x2 );

	return m * ( xValue - x1 ) + z1;
}