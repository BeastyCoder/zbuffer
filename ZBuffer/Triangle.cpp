#include "Triangle.h"

double Triangle::getUpperScanLine( )
{
	double triangleYMax = p[0].arr[1];

	for ( int i = 0; i < 3; i++ )
	{
		if ( p[i].arr[1] > triangleYMax )
			triangleYMax = p[i].arr[1];
	}

	if ( triangleYMax > 1 )
		triangleYMax = 1;

	return triangleYMax;
}

double Triangle::getBottomScanLine( )
{
	double triangleYMin = p[0].arr[1];

	for ( int i = 0; i < 3; i++ )
	{
		if ( p[i].arr[1] < triangleYMin )
			triangleYMin = p[i].arr[1];
	}

	if ( triangleYMin < -1 )
		triangleYMin = -1;

	return triangleYMin;
}

int Triangle::getTrianglePeakIndex()
{
	double triangleYMax = p[0].arr[1];
	int peakIndex = 0;

	for ( int i = 0; i < 3; i++ )
	{
		if ( p[i].arr[1] > triangleYMax )
		{
			triangleYMax = p[i].arr[1];
			peakIndex = i;
		}
	}

	return peakIndex;
}