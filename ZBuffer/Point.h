#ifndef POINT_H
#define POINT_H

class Point
{
public:
	double arr[3];

	Point( );
	Point( double x, double y, double z );

	static Point getIntersectionPointBetweenAtY( Point p1, Point p2, double yValue );
	static double getZValue( Point p1, Point p2, double xValue );
};

#endif