#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "Point.h"

class Triangle
{
public:
	Point p[3];
	int color[3];

	double getUpperScanLine( );
	double getBottomScanLine( );
	int getTrianglePeakIndex( );
};

#endif