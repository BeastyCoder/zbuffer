#define _SCL_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <stack>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include "bitmap_image.hpp"
#include "Point.h"
#include "Triangle.h"
using namespace std;

int screenHeight, screenWidth;
double xLimit, yLimit, frontZLimit, rearZLimit;
double top_scanline = 1;
double bottom_scanline = -1;
double** zBuffer;
int*** frameBuffer;

vector<Triangle> triangles;

int getScanRow(double yValue)
{
	return ( int ) ( -yValue * ( ( screenHeight - 1 ) / 2 ) + ( ( screenHeight - 1 ) / 2 ) );
}

int getScanColumn(double xValue)
{
	return ( int ) ( xValue * ( ( screenWidth - 1 ) / 2 ) + ( ( screenWidth - 1 ) / 2 ) );
}

double getYValue(int rowIndex)
{
	return ( -rowIndex / ( ( screenHeight - 1 ) / 2.0 ) + 1 );
}

double getXValue(int colIndex)
{
	return ( colIndex / ( ( screenWidth - 1 ) / 2.0 ) - 1 );
}

void read_data( )
{
	int line_num = 0;

	ifstream scene;
	scene.open( "config.txt" );

	double value;

	scene >> screenWidth >> screenHeight >> xLimit >> yLimit >> frontZLimit >> rearZLimit;


	cout << "Screen is " << screenWidth << " " << screenHeight
		<< " " << xLimit << " " << yLimit << " " << frontZLimit << " " << rearZLimit << endl;
	scene.close( );


	ifstream objectScene;
	objectScene.open( "stage3.txt" );
	double valueX, valueY, valueZ;
	vector<Point> points;

	while ( objectScene >> valueX >> valueY >> valueZ )
	{
		Point p( valueX, valueY, valueZ );
		points.push_back( p );
	}

	srand( time( 0 ) );
	for ( int i = 0; i < points.size( ); i += 3 )
	{
		Triangle t;
		t.p[0] = points[i];
		t.p[1] = points[i + 1];
		t.p[2] = points[i + 2];


		t.color[0] = abs( rand( ) ) % 256;
		t.color[1] = abs( rand( ) ) % 256;
		t.color[2] = abs( rand( ) ) % 256;

		triangles.push_back( t );
	}

	for ( int i = 0; i < triangles.size( ); i++ )
	{
		cout << triangles[i].color[0] << " " << triangles[i].color[1] << " " << triangles[i].color[2] << endl;
	}

	objectScene.close( );
}

void initialize_z_buffer_and_frameBuffer( )
{
	double dx = 2 / screenWidth;
	double dy = 2 / screenHeight;

	zBuffer = new double*[screenWidth];

	for ( int i = 0; i < screenWidth; i++ )
	{
		zBuffer[i] = new double[screenHeight];
		for ( int j = 0; j < screenHeight; j++ )
		{
			zBuffer[i][j] = rearZLimit;
		}
	}

	frameBuffer = new int**[screenHeight];

	for ( int i = 0; i < screenHeight; i++ )
	{
		frameBuffer[i] = new int*[screenWidth];

		for ( int j = 0; j < screenWidth; j++ )
		{
			frameBuffer[i][j] = new int[3];

			for ( int k = 0; k < 3; k++ )
				frameBuffer[i][j][k] = 0;
		}
	}

	bitmap_image image( screenWidth, screenHeight );

	for ( int i = 0; i < screenWidth; i++ )
	{
		for ( int j = 0; j < screenHeight; j++ )
		{
			image.set_pixel( i, j, 0, 0, 0 );
		}
	}

	image.save_image( "output.bmp" );
}

void apply_procedure()
{
	for (int i = 0; i < triangles.size(); i++)
	{
		Triangle currentTriangle = triangles[i];

		double yMax = currentTriangle.getUpperScanLine();
		double yMin = currentTriangle.getBottomScanLine();

		int upperRow = getScanRow(yMax);
		int bottomRow = getScanRow(yMin);

		for (int j = upperRow; j < bottomRow; j++)
		{
			double curY = getYValue(j);

			int trianglePeak = currentTriangle.getTrianglePeakIndex();

			int otherPoint = 3 - trianglePeak;
			int anotherPoint = 3 - trianglePeak - otherPoint;

			Point intersectionPoint1 = Point::getIntersectionPointBetweenAtY(currentTriangle.p[trianglePeak], currentTriangle.p[otherPoint], curY);
			Point intersectionPoint2 = Point::getIntersectionPointBetweenAtY( currentTriangle.p[trianglePeak], currentTriangle.p[anotherPoint], curY );

			double minX = (intersectionPoint1.arr[0] < intersectionPoint2.arr[0]) ? intersectionPoint1.arr[0] : intersectionPoint2.arr[0];
			double maxX = (intersectionPoint1.arr[0] > intersectionPoint2.arr[0]) ? intersectionPoint1.arr[0] : intersectionPoint2.arr[0];

			Point minIntersectionPoint = (intersectionPoint1.arr[0] < intersectionPoint2.arr[0]) ? intersectionPoint1: intersectionPoint2;
			Point maxIntersectionPoint = (intersectionPoint1.arr[0] > intersectionPoint2.arr[0]) ? intersectionPoint1 : intersectionPoint2;

			if (minX < -1)
				minX = -1;
			else if (minX > 1)
				break;

			if (maxX > 1)
				maxX = 1;
			else if (maxX < -1)
				break;

			int leftScanRow = getScanColumn(minX);
			int rightScanRow = getScanColumn(maxX);

			for (int k = leftScanRow; k < rightScanRow; k++)
			{
				double curZ = Point::getZValue(minIntersectionPoint, maxIntersectionPoint, getXValue(k));

				if (curZ < zBuffer[j][k])
				{
					zBuffer[j][k] = curZ;

					frameBuffer[k][j][0] = currentTriangle.color[0];
					frameBuffer[k][j][1] = currentTriangle.color[1];
					frameBuffer[k][j][2] = currentTriangle.color[2];
				}
			}
		}
	}
}

void save()
{
	bitmap_image image(screenHeight, screenWidth);

	for ( int i = 0; i < screenHeight; i++ )
	{
		for ( int j = 0; j < screenWidth; j++ )
		{
			image.set_pixel(i, j, frameBuffer[i][j][0], frameBuffer[i][j][1], frameBuffer[i][j][2]);
		}
	}

	image.save_image("output.bmp");

	ofstream out("z_buffer.txt");

	for ( int i = 0; i < screenWidth; i++ )
	{
		for ( int j = 0; j < screenHeight; j++ )
		{
			out << zBuffer[i][j] << " ";
		}
		out << endl;
	}
}

void free_memory()
{
	for (int i = 0; i < screenWidth; i++)
	{
		delete zBuffer[i];
	}

	delete zBuffer;

	for (int i = 0; i < screenHeight; i++)
	{
		for (int j = 0; j < screenWidth; j++)
			delete frameBuffer[i][j];

		delete frameBuffer[i];
	}

	delete frameBuffer;
}

int main()
{
	read_data();
	initialize_z_buffer_and_frameBuffer();
	apply_procedure();
	save();
	free_memory();
}
